#include "caesar_cipher.h"

unsigned short int 
machine_index_get(
    pthread_t tid,
    const program_data *p_data)
{
    unsigned short int index;
    for(index=0;index<MACHINE_NUMBER;index++){
        if(tid == p_data->machine_thread[index])
            break;
    }
    return index;
}

int 
en_de_cryption(
    program_data *p_data,
    const file_info *current_file, 
    unsigned short int machine_index)
{
    FILE *p_file_r = NULL;
    FILE *p_file_w = NULL;
    int i,ret,paragraph;
    long int nLeft;
    char *file_path_temp;
    char c_buffer, s_buffer[CRYPT_BUFFER];
    file_signature signature;
    file_path_temp = (char*)calloc(MAX_PATH_LENGTH,sizeof(char));
    switch(current_file->file_state){
        case ORIGINAL:{
            /*Create encrypt file info*/
            memset(file_path_temp,'\0',MAX_PATH_LENGTH);
            strcat(file_path_temp,current_file->file_path);
            strcat(file_path_temp,".dzs");
            /*Open file source, file encrypt */
            p_file_r = fopen(current_file->file_path,"rb");
            if(NULL==p_file_r){
                perror("ERROR/en_de_cryption/read_file");
                break;
            }
            p_file_w = fopen(file_path_temp,"wb+");
            /*Create signature*/
            signature.file_size = current_file->file_size;
            strcpy(signature.opcode,"DZS ENCRYPTED");
            strcpy(signature.file_name, current_file->file_name);
            /*Write signature*/
            ret = fwrite(&signature,sizeof(signature),1,p_file_w);
            nLeft = current_file->file_size;
            paragraph = nLeft/CRYPT_BUFFER;
            /*Write file data*/
            p_data->m_process_info[machine_index].total_work = nLeft;
            while(paragraph>0){
                ret = fread(&s_buffer,1,CRYPT_BUFFER,p_file_r);
                for(i=0;i<CRYPT_BUFFER;i++){
                    s_buffer[i] = (s_buffer[i]+ p_data->caesar_key)%256;
                }
                ret = fwrite(&s_buffer,1,CRYPT_BUFFER,p_file_w);
                nLeft = nLeft - ret;
                paragraph = paragraph - 1;
                p_data->m_process_info[machine_index].remain_work = nLeft;
            }
            while(nLeft>0){
                ret = fread(&c_buffer,1,sizeof(char),p_file_r);
                c_buffer = (c_buffer +p_data->caesar_key)%256;
                ret = fwrite(&c_buffer,1,sizeof(char),p_file_w);
                nLeft = nLeft -1;
            }
            p_data->m_process_info[machine_index].remain_work = current_file->file_size;
            fclose(p_file_r);
            fclose(p_file_w);
        }break;
        case ENCRYPTED:{
            unsigned int string_length;
            string_length = strlen(current_file->file_path);
            p_file_r = fopen(current_file->file_path,"rb");
            if(NULL==p_file_r){
                perror("ERROR/en_de_cryption/read_file");
                break;
            }
            ret = fread(&signature,1,sizeof(file_signature),p_file_r);
            memset(file_path_temp,'\0',MAX_PATH_LENGTH);
            strncat(file_path_temp,current_file->file_path,string_length - strlen(current_file->file_name));
            if(0!=strcmp(current_file->file_name,signature.file_name)){
                strcat(file_path_temp,signature.file_name);
            }else {
                strcat(file_path_temp,"BACKUP - ");
                strcat(file_path_temp,signature.file_name);
            }
            p_file_w = fopen(file_path_temp,"wb+");
            nLeft = signature.file_size;
            paragraph = nLeft/CRYPT_BUFFER;
            p_data->m_process_info[machine_index].total_work = nLeft;
            while(paragraph>0){
                ret = fread(&s_buffer,1,CRYPT_BUFFER,p_file_r);
                for(i=0;i<CRYPT_BUFFER;i++){
                    s_buffer[i] = (s_buffer[i]- p_data->caesar_key)%256;
                }
                ret = fwrite(&s_buffer,1,CRYPT_BUFFER,p_file_w);
                nLeft = nLeft - ret;
                paragraph = paragraph - 1;
                p_data->m_process_info[machine_index].remain_work = nLeft;
            }
            while(nLeft>0){
                ret = fread(&c_buffer,1,sizeof(char),p_file_r);
                c_buffer = (c_buffer - p_data->caesar_key)%256;
                ret = fwrite(&c_buffer,1,sizeof(char),p_file_w);
                nLeft = nLeft -1;
            }
            p_data->m_process_info[machine_index].remain_work = signature.file_size;
            fclose(p_file_r);
            fclose(p_file_w);
        }break;
        default:{}
    }
    return 0;
}

int folder_scan(char *folder_path, program_data *p_data) 
{
    struct dirent *dirp;
    int result;
    long int size_file;
    DIR *dir;
    dir = opendir(folder_path);
    chdir(folder_path);
    while ((dirp = readdir(dir)) != NULL) {
        if(dirp->d_type !=DT_DIR){
            /*check file is not hidden*/
            result = file_is_hidden(dirp->d_name);
            if (0 != result){
                char *path_file = NULL;
                unsigned int length_1, length_2;
                length_1 = strlen(folder_path);
                length_2 = strlen(dirp->d_name);
                path_file = (char*)calloc(length_1+length_2+2, sizeof(char));
                if (NULL == path_file){
                    perror("ERROR/file_scan/path_file");
                    return -1;
                } else {
                    /*Add file info to the playlist*/
                    size_file = file_size_get(dirp->d_name);
                    strcat(path_file,folder_path);
                    strcat(path_file,"/");
                    strcat(path_file, dirp->d_name);
                    /*Check file signature*/
                    result = file_signature_check(path_file,size_file);
                    if(0==result){
                        task_add_rear(
                            p_data->decrypt_list, 
                            dirp->d_name, 
                            path_file, 
                            size_file, ENCRYPTED);
                    }else{
                        task_add_rear(
                            p_data->encrypt_list, 
                            dirp->d_name, 
                            path_file, 
                            size_file, ORIGINAL);
                    }           
                    SAFE_FREE(path_file);
                }
            }
        }
    }
    chdir("..");
    closedir(dir);
    return 0;
}

int 
task_add_rear(
    task_list *current_list, 
    char  *file_name, 
    char *file_path, 
    long int file_size, 
    int state)
{
    int length_name = 0;
    int length_path = 0;
    task_node *new_task = NULL;
    length_name = strlen(file_name);
    length_path = strlen(file_path);
    new_task = (task_node*)calloc(1, sizeof(task_node));
    new_task->data.file_name = (char*)calloc(length_name + 1, sizeof(char));
    new_task->data.file_path = (char*)calloc(length_path + 1, sizeof(char));
    if (NULL == new_task || 
        NULL == new_task->data.file_name || 
        NULL == new_task->data.file_path){
        perror("ERROR/task_add_rear/creat_node");
        return ERROR_MEMORY_ALLOWCATION;
    } else {
        strcpy(new_task->data.file_name, file_name);
        strcpy(new_task->data.file_path, file_path);
        new_task->data.file_size = file_size;
        new_task->data.file_state = state;
        new_task->next = NULL;
        new_task->handle_state = READY;
        if (0 == task_list_is_empty(current_list)){
            current_list->front = new_task;
            current_list->rear = new_task;
        } else {
            current_list->rear->next = new_task;
            current_list->rear = new_task;
        }
    }
    return 0;
}

task_node *task_search(char *task_name,task_list *c_list)
{
    short int result;
    task_node *p_node = NULL;
    p_node = c_list->front;
    while(NULL!=p_node){
        result = strcmp(task_name,p_node->data.file_name);
        if(result==0){
            return p_node;
        }
        p_node = p_node->next;
    }
    return NULL;
}

int file_signature_check(char *file_path, long int file_size)
{
    int result = -1;
    int ret = 0;
    if(file_size>sizeof(file_signature)){
        file_signature block;
        FILE *p_file = NULL;
        p_file = fopen(file_path,"rb");
        ret = fread(&block,1,sizeof(file_signature),p_file);
        if(ret == sizeof(file_signature)){
            result = strcmp(block.opcode,"DZS ENCRYPTED");
        }
        fclose(p_file);
    }
    return result;
}

int list_view_refresh(task_list *c_list, GtkListStore *c_list_store)
{
    gint count_task = 0;
    char file_state[16];
    char string_file_size[20];
    gtk_list_store_clear(c_list_store);
    task_node *p_index = c_list->front;
    while (NULL != p_index){
        GtkTreeIter iter;
        count_task++;
        // printf("INTER : %d\n",iter.stamp);
        gtk_list_store_append(c_list_store, &iter);
        memset(file_state,'\0',16);
        switch(p_index->handle_state){
            case READY: strcpy(file_state,"1-READY");break;
            case ENCRYPTING: strcpy(file_state,"3-ENCRYPTING");break;
            case DECRYPTING: strcpy(file_state,"3-DECRYPTING");break;
            case DONE: strcpy(file_state,"2-DONE");break;
            default:{}
        }
        file_size_convert(p_index->data.file_size,string_file_size);
        gtk_list_store_set( c_list_store,
                            &iter,
                            0, count_task,
                            1, file_state,
                            2, p_index->data.file_name,
                            3, string_file_size,
                            4, p_index->data.file_path, -1);
        p_index = p_index->next;
    }
    return 0;
}

int file_size_convert(long int file_size, char *result_buffer)
{
    double value = 0.0;
    memset(result_buffer,'\0',sizeof(result_buffer)/1);
    if(file_size< KILOBYTE){
        sprintf(result_buffer,"%ld bytes", file_size);
    }
    else if(file_size < MEGABYTE){
        value = (double)file_size/KILOBYTE;
        sprintf(result_buffer,"%0.2f kB",value);
    }
    else if(file_size < GIGABYTE){
        value = (double)file_size/MEGABYTE;
        sprintf(result_buffer,"%0.2f MB",value);
    }
    else{
        value = (double)file_size/GIGABYTE;
        sprintf(result_buffer,"%0.2f GB",value);
    }
    return 0;
}

int task_front_remove(task_list *current_list)
{
    if (NULL != current_list->front){
        task_node *p_free = NULL; 
        p_free = current_list->front;
        current_list->front = current_list->front->next;
        SAFE_FREE(p_free->data.file_name);
        SAFE_FREE(p_free->data.file_path);
        SAFE_FREE(p_free);
        if(NULL==current_list->front ){
            current_list->rear = NULL;
        }
    }
    return 0;
}

int file_is_hidden(const char *file_name)
{
    if(file_name[0]=='.'){
        return 0;
    }else {
        return -1;
    }
}

int task_list_show(const task_list *current_list)
{
    int count_task = 0;
    task_node *p_task = current_list->front;
    if (0 != task_list_is_empty(current_list)){
        while (NULL != p_task){
            count_task = count_task + 1;
            printf("%d: %s\n", count_task, p_task->data.file_path);
            p_task = p_task->next;
        }
    } else {
        printf("LIST EMPTY\n");
    }
    return 0;
}

int task_list_count(const task_list *current_list)
{
    int count_task = 0;
    task_node *p_task = current_list->front;
    if (0 != task_list_is_empty(current_list)){
        while (NULL != p_task){
            count_task = count_task + 1;
            p_task = p_task->next;
        }
    }
    return count_task;
}

int task_list_free(task_list *current_list)
{
    if (0 != task_list_is_empty(current_list)){
        task_node *p_free = NULL;
        while (current_list->front != current_list->rear){
            p_free = current_list->front;
            current_list->front = current_list->front->next;
            SAFE_FREE(p_free->data.file_name);
            SAFE_FREE(p_free->data.file_path);
            SAFE_FREE(p_free);
        }
        SAFE_FREE(current_list->front->data.file_name);
        SAFE_FREE(current_list->front->data.file_path);
        SAFE_FREE(current_list->front);
        current_list->front = NULL;
        current_list->rear = NULL;
    }
    return 0;
}

int task_list_is_empty(const task_list *current_list)
{
    if (NULL == current_list->front){
        return 0;
    }else{
        return -1;
    }
}

long int file_size_get(const char *file_path)
{
    long int file_size;
    FILE *p_file = NULL;
    p_file = fopen(file_path, "rb");
    if(NULL == p_file){
        perror("ERROR/file_size_get/read_file");
        return -1;
    }
    fseek(p_file, 0, SEEK_END);
    file_size = ftell(p_file);
    fclose(p_file);
    return file_size;
}

int task_remove(char *file_path, task_list *c_list)
{
    int result_check;
    task_node *p_node = c_list->front;
    task_node *pre_p_node = NULL;
    while(NULL!=p_node){
        result_check = strcmp(file_path,p_node->data.file_path);
        if(0==result_check){
            if(c_list->front == c_list->rear){
                SAFE_FREE(p_node->data.file_name);
                SAFE_FREE(p_node->data.file_path);
                SAFE_FREE(p_node);
                c_list->front = NULL;
                c_list->rear = NULL;
            }
            else if(p_node == c_list->front && p_node != c_list->rear){
                c_list->front = p_node->next;
                SAFE_FREE(p_node->data.file_name);
                SAFE_FREE(p_node->data.file_path);
                SAFE_FREE(p_node);
            }
            else if(p_node != c_list->front && p_node == c_list->rear){
                pre_p_node->next = NULL;
                c_list->rear = pre_p_node;
                SAFE_FREE(p_node->data.file_name);
                SAFE_FREE(p_node->data.file_path);
                SAFE_FREE(p_node);
            }
            else{
                pre_p_node->next = p_node->next;
                SAFE_FREE(p_node->data.file_name);
                SAFE_FREE(p_node->data.file_path);
                SAFE_FREE(p_node);
            }
            return 0;
        }else{
            pre_p_node = p_node;
            p_node = p_node->next;
        }
    }
    return -1;
}
