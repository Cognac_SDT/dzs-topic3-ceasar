#include "user_interface.h"


gboolean dialog_finish (program_data *p_data) {
    gtk_widget_show(p_data->window_dialog_finish);
    g_source_remove(p_data->id_gsource_dialog);
    sem_post(&sem_dialog_finish);
    return TRUE;
}

gboolean dialog_rescan (program_data *p_data) {
    gtk_widget_show(p_data->window_dialog_rescan);
    g_source_remove(p_data->id_gsource_dialog_rescan);
    sem_post(&sem_dialog_rescan);
    return TRUE;
}
gboolean refresh_tree_view (program_data *p_data) {
    list_view_refresh(p_data->encrypt_list, p_data->liststore_encrypt);
    list_view_refresh(p_data->decrypt_list, p_data->liststore_decrypt);
    g_source_remove(p_data->id_gsource_refresh_ui);
    sem_post(&sem_refresh_view);
    return TRUE;
}

gboolean fill (program_data *p_data) {
    double done_work = 0.0;
    int i;
    for (i = 0; i < MACHINE_NUMBER; i++) {
        if (0.0 != p_data->m_process_info[i].total_work)
        {
            done_work = p_data->m_process_info[i].total_work - p_data->m_process_info[i].remain_work;
            p_data->m_process_info[i].fraction = done_work / p_data->m_process_info[i].total_work;
            gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR (p_data->progress_bar[i]), p_data->m_process_info[i].fraction);
            gtk_label_set_label(p_data->machine_task[i], p_data->m_process_info[i].name);
            gtk_label_set_label(p_data->machine_state[i], p_data->m_process_info[i].state);
        }
    }
    return TRUE;
}

gboolean fc_choose_folder_set(GtkFileChooserButton *chooser,program_data *p_data) 
{
    gchar *folderpath;
    GtkFileChooser *folderchooser = GTK_FILE_CHOOSER(chooser);
    folderpath = gtk_file_chooser_get_filename(folderchooser);
    printf("FOLDER:%s\n", folderpath);
    memset(p_data->folder_chooser_path, '\0', 500);
    sprintf(p_data->folder_chooser_path, "%s", folderpath);
    return TRUE;
}

gboolean b_scan_clicked(GtkButton *button,program_data* p_data) 
{
    if(0==p_data->num_total_work){
        if (0 != strlen(p_data->folder_chooser_path)) {
            p_data->num_total_work = 0;
            p_data->num_done_work = 0;
            p_data->num_total_en_work = 0;
            p_data->num_done_en_work = 0;
            p_data->num_total_de_work = 0;
            p_data->num_done_de_work = 0;
            /*refresh task lists*/
            task_list_free(p_data->encrypt_list);
            task_list_free(p_data->decrypt_list);
            /*scan file, add to task lists*/
            folder_scan(p_data->folder_chooser_path, p_data);
            /*refesh display task lists*/
            list_view_refresh(p_data->encrypt_list, p_data->liststore_encrypt);
            list_view_refresh(p_data->decrypt_list, p_data->liststore_decrypt);
            gtk_widget_show_all(p_data->window_dialog_scan);
            //task_list_show(p_data->encrypt_list);
            //task_list_show(p_data->decrypt_list);
        } else {
            gtk_widget_show_all(p_data->window_dialog_directory);
            printf("PLEASE CHOOSE FOLDER\n");
        }
    }else {
        gtk_widget_show_all(p_data->window_dialog_wait);
        printf("ACTION IS BLOCKED! TASK LIST HANDLING IS NOT DONE, TRY AGAIN\n");
    }
    return TRUE;
}

gboolean b_encrypt_clicked(GtkButton *button,program_data* p_data) 
{
    sem_post(&sem_encrypt);
    return TRUE;
}

gboolean b_decrypt_clicked(GtkButton *button,program_data* p_data) 
{
    sem_post(&sem_decrypt);
    return TRUE;
}

gboolean b_caesar_key_changed(GtkButton *button,program_data* p_data) 
{
    if(0==p_data->num_total_work){
        gint value;
        value = gtk_spin_button_get_value_as_int(p_data->sb_caesar_key);
        p_data->caesar_key = value;
        printf("UPDATE KEY IS %d\n", value);
    }else{
        gtk_spin_button_set_value(p_data->sb_caesar_key,p_data->caesar_key);
        gtk_widget_show_all(p_data->window_dialog_wait);
        printf("ACTION IS BLOCKED! TASK LIST HANDLING IS NOT DONE, TRY AGAIN\n");
    }
    return TRUE;
}

gboolean b_about_clicked(GtkButton *button,program_data* p_data) 
{
    gtk_widget_show_all(p_data->window_dialog_about);
    return TRUE;
}

gboolean b_finish_ok_clicked(GtkWidget *window,program_data* p_data) 
{
    gtk_widget_hide(p_data->window_dialog_finish);
    return TRUE;
}

gboolean b_rescan_ok_clicked(GtkWidget *window,program_data* p_data) 
{
    gtk_widget_hide(p_data->window_dialog_rescan);
    return TRUE;
}

gboolean b_scan_ok_clicked(
    GtkWidget *window,
    program_data* p_data) {
    gtk_widget_hide(p_data->window_dialog_scan);
    return TRUE;
}

gboolean b_directory_ok_clicked(GtkWidget *window,program_data* p_data) 
{
    gtk_widget_hide(p_data->window_dialog_directory);
    return TRUE;
}

gboolean b_about_ok_clicked(GtkWidget *window,program_data* p_data) 
{
    gtk_widget_hide(p_data->window_dialog_about);
    return TRUE;
}

gboolean b_wait_ok_clicked(GtkWidget *window,program_data* p_data) 
{
    gtk_widget_hide(p_data->window_dialog_wait);
    return TRUE;
}

void view_popup_menu_do_remove_encrypt(GtkWidget *menuitem,program_data *p_data) 
{
    GtkTreeModel *model;
    GtkTreeIter   iter;
    GtkTreeView *treeview = GTK_TREE_VIEW(p_data->w_encrypt_list);
    GtkTreeSelection *selection = gtk_tree_view_get_selection(treeview);
    if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
        gint id;
        gchar *name, *state, *path, *size;
        gtk_tree_model_get(
            model, &iter,
            0, &id,
            1, &state,
            2, &name,
            3, &size,
            4, &path, -1);
        task_remove(path,p_data->encrypt_list);
        list_view_refresh(p_data->encrypt_list, p_data->liststore_encrypt);
    }
}

void view_popup_menu_list_encrypt(
    GtkWidget *treeview,
    GdkEventButton *event,
    program_data *p_data) 
{
    GtkWidget *menu, *menu_remove;
    menu = gtk_menu_new();
    menu_remove = gtk_menu_item_new_with_label("remove");
    g_signal_connect(
        menu_remove,
        "activate",
        (GCallback)view_popup_menu_do_remove_encrypt,
        p_data);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_remove);
    if(0==p_data->num_total_work){
        gtk_widget_show_all(menu);
    }else {
        gtk_widget_hide(menu);
    }
    gtk_menu_popup_at_pointer(GTK_MENU(menu), NULL);
}

gboolean view_onButtonPressed_list_encrypt(
    GtkWidget *treeview,
    GdkEventButton *event,
    program_data *p_data) {
    if (event->type == GDK_BUTTON_PRESS  &&  event->button == 3) {
        GtkTreeSelection *selection;
        GtkTreeModel *model;
        GtkTreeIter   iter;
        selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
        if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
            view_popup_menu_list_encrypt(treeview, event, p_data);
            return TRUE;
        }
    }
    return FALSE;
}

void view_popup_menu_do_remove_decrypt(GtkWidget *menuitem,program_data *p_data) 
{
    GtkTreeModel *model;
    GtkTreeIter   iter;
    GtkTreeView *treeview = GTK_TREE_VIEW(p_data->w_decrypt_list);
    GtkTreeSelection *selection = gtk_tree_view_get_selection(treeview);
    if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
        gint id;
        gchar *name, *state, *path, *size;
        gtk_tree_model_get(
            model, &iter,
            0, &id,
            1, &state,
            2, &name,
            3, &size,
            4, &path, -1);
        task_remove(path,p_data->decrypt_list);
        list_view_refresh(p_data->decrypt_list, p_data->liststore_decrypt);
    }
}

void 
view_popup_menu_list_decrypt(
    GtkWidget *treeview,
    GdkEventButton *event,
    program_data *p_data) 
{
    GtkWidget *menu, *menu_remove;
    menu = gtk_menu_new();
    menu_remove = gtk_menu_item_new_with_label("remove");
    g_signal_connect(
        menu_remove,
        "activate",
        (GCallback)view_popup_menu_do_remove_decrypt,
        p_data);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_remove);
    if(0==p_data->num_total_work){
        gtk_widget_show_all(menu);
    }else {
        gtk_widget_hide(menu);
    }
    gtk_menu_popup_at_pointer(GTK_MENU(menu), NULL);
}

gboolean 
view_onButtonPressed_list_decrypt(
    GtkWidget *treeview,
    GdkEventButton *event,
    program_data *p_data) {
    if (event->type == GDK_BUTTON_PRESS  &&  event->button == 3) {
        GtkTreeSelection *selection;
        GtkTreeModel *model;
        GtkTreeIter   iter;
        selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
        if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
            view_popup_menu_list_decrypt(treeview, event, p_data);
            return TRUE;
        }
    }
    return FALSE;
}
void on_window_main_destroy(GtkWidget *window_main,program_data *p_data) 
{
    sem_destroy(&sem_ready);
    sem_destroy(&sem_task);
    sem_destroy(&sem_encrypt);
    sem_destroy(&sem_decrypt);
    sem_destroy(&sem_encrypt_finish);
    sem_destroy(&sem_decrypt_finish);
    sem_destroy(&sem_refresh_view);
    sem_destroy(&sem_dialog_finish);
    sem_destroy(&sem_dialog_rescan);
    pthread_mutex_destroy(&mutex_machine);
    pthread_mutex_destroy(&mutex_staff);
    
    SAFE_FREE(p_data->folder_chooser_path);
    int i;
    for (i = 0; i < MACHINE_NUMBER; i++) {
        SAFE_FREE(machine_buffer[i].file_path);
        SAFE_FREE(machine_buffer[i].file_path);
    }
    task_list_free(p_data->encrypt_list);
    task_list_free(p_data->decrypt_list);
    SAFE_FREE(p_data->encrypt_list);
    SAFE_FREE(p_data->decrypt_list);
    SAFE_FREE(p_data);

    gtk_main_quit();
}