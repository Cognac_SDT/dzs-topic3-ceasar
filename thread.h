#ifndef _THREAD_H_
#define _THREAD_H_

#include "define.h"
#include "caesar_cipher.h"
#include "user_interface.h"

/*buffer stores process infor of threads machine*/
file_info machine_buffer[MACHINE_NUMBER];

/*
** machine get task information in the buffer and call function process
*/
void *machine_handle(void *gp);
/*
** staff get task from encrypt list and puts it in machine buffer
*/
void *staff_handle_encrypt(void *c_task_list);
/*
** staff get task from decrypt list and puts it in machine buffer
*/
void *staff_handle_decrypt(void *c_task_list);

#endif