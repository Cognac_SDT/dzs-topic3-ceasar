#ifndef _DEFINE_H_
#define _DEFINE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>

/*String length*/
#define MACHINE_NUMBER 4
#define MAX_NAME_LENGTH 256
#define MAX_PATH_LENGTH 4096
#define MAX_STATE_LENGTH 20
#define MAX_OPCODE_LENGTH 20

/*Storage unit*/
#define KILOBYTE 1024.0
#define MEGABYTE 1048576.0
#define GIGABYTE 1073741824.0

/*File crypt state*/
#define ORIGINAL 0
#define ENCRYPTED 1

/*Handle state*/
#define READY 2
#define ENCRYPTING 3
#define DECRYPTING 4
#define DONE 5

/*Size block r/w file*/
#define CRYPT_BUFFER 512

/*macro safe when free*/
#define SAFE_FREE(pointer) {if(NULL!=pointer){free(pointer); pointer=NULL;}}
#define ERROR_MEMORY_ALLOWCATION -1

/*structure is saved at the begin encrypted file*/
typedef struct _signature
{
    long int file_size;
    char opcode[MAX_OPCODE_LENGTH];
    char file_name[MAX_NAME_LENGTH];
}file_signature;

/*information on the threads machine is working*/
typedef struct _process_info
{
    double fraction;
    double total_work;
    double remain_work;
    char name[MAX_NAME_LENGTH];
    char state[MAX_STATE_LENGTH];
} process_info;

/*file information to be encrypt/decrypt*/
typedef struct _data
{
    long int file_size;
    unsigned int file_state;
    char *file_name;
    char *file_path;
} file_info;

typedef struct _node
{
    file_info data;
    unsigned int handle_state;
    struct _node *next;
} task_node;

typedef struct _list
{
    task_node *front;
    task_node *rear;
} task_list;

/*UI widget, data to share in the program*/
typedef struct {
    int caesar_key;
    int num_total_work;
    int num_done_work;
    int num_total_en_work;
    int num_done_en_work;
    int num_total_de_work;
    int num_done_de_work;
    int task_in;
    int task_out;
    char *folder_chooser_path;
    pthread_t 
        staff_encrypt_thread, 
        staff_decrypt_thread,
        machine_thread[MACHINE_NUMBER],
        refresh_ui;
    task_list 
        *encrypt_list, 
        *decrypt_list;
    process_info 
        m_process_info[MACHINE_NUMBER];
    GtkWidget   
        *window_main,
        *window_dialog_finish,
        *window_dialog_rescan,
        *window_dialog_scan,
        *window_dialog_about,
        *window_dialog_directory,
        *window_dialog_wait,
        *w_encrypt_list,
        *w_decrypt_list,
        *fc_choose_folder,
        *progress_bar[MACHINE_NUMBER];          
    GtkLabel    
        *machine_task[MACHINE_NUMBER],
        *machine_state[MACHINE_NUMBER];
    GtkButton   
        *b_encrypt,
        *b_decrypt,
        *b_scan,
        *b_about,
        *b_finish_ok,
        *b_rescan_ok,
        *b_scan_ok,
        *b_about_ok,
        *b_directory_ok,
        *b_wait_ok;
    GtkSpinButton 
        *sb_caesar_key;
    GtkMenuButton 
        *mb_setting;
    GtkTreeViewColumn   
        *en_column[5],
        *de_column[5];
    GtkListStore    
        *liststore_encrypt, 
        *liststore_decrypt;
    guint 
        id_gsource, 
        id_gsource_dialog, 
        id_gsource_refresh_ui,
        id_gsource_dialog_rescan;
} program_data;

/* semaphore, conditional variable, mutex used for sync threads*/
sem_t 
    sem_ready, 
    sem_task, 
    sem_encrypt,
    sem_decrypt,
    sem_encrypt_finish,
    sem_decrypt_finish,
    sem_refresh_view, 
    sem_dialog_finish, 
    sem_dialog_rescan;
pthread_mutex_t 
    mutex_staff,
    mutex_machine;

#endif



