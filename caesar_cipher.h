#ifndef _SAESAR_CIPHER_H_
#define _SAESAR_CIPHER_H_

#include "define.h"

/*
** scans all files in the directory (not scanning subfolders, hidden files), check 
** the file's encryption signature and then add the information to the task lists.
*/
int 
folder_scan(
    char *folder_path, 
    program_data *p_data);
/*
** get the size of the file
*/
long int file_size_get(const char *file_path);
/*
** check the file's encryption signature, If the file contains a signature, it's encrypted.
*/
int file_signature_check(char *file_path, long int file_size);
/*
** refesh task list display
*/
int list_view_refresh(task_list *c_list, GtkListStore *c_list_store);
/*
** convert number file size to string
*/
int file_size_convert(long int file_size, char *result_buffer);
/*
** check this file is hidden file?
*/
int file_is_hidden(const char *file_name);
/*
** check this list is empty list?
*/
int task_list_is_empty(const task_list *current_list);
/*
** add new task to rear of task list
*/
int 
task_add_rear(
    task_list *current_list, 
    char  *file_name, char *file_path, 
    long int file_size, int state);
/*
** delete first task in the task list
*/
int task_front_remove(task_list *current_list);
/*
** delete task in the task list by file path 
*/
int task_remove(char *file_path, task_list *c_list);
/*
** show all task list on terminal
*/
int task_list_show(const task_list *current_list);
/*
** count number task on list
*/
int task_list_count(const task_list *current_list);
/*
** delete all task in list
*/
int task_list_free(task_list *current_list);
/*
** search task in the list via nam task
*/
task_node *task_search(char *task_name,task_list *c_list);
/*
** get the index ò machine threads
*/
unsigned short int 
machine_index_get(
    pthread_t tid,
    const program_data *p_data);
/*
** file encrypt and decrypt
*/
int 
en_de_cryption(
    program_data *p_data,
    const file_info *current_file, 
    unsigned short int machine_index);

#endif