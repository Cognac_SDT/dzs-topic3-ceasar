CC= gcc
RM= rm
cipher : main.o caesar_cipher.o thread.o user_interface.o
	$(CC) -o cipher main.o caesar_cipher.o thread.o user_interface.o `pkg-config --cflags --libs gtk+-3.0`
	$(RM) -rf  *.o                                 
main.o: main.c
	$(CC) -c -Wall main.c `pkg-config --cflags --libs gtk+-3.0`
caesar_cipher.o: caesar_cipher.c
	$(CC) -c -Wall caesar_cipher.c `pkg-config --cflags --libs gtk+-3.0`
thread.o: thread.c
	$(CC) -c -Wall -pthread thread.c `pkg-config --cflags --libs gtk+-3.0`
user_interface.o: user_interface.c
	$(CC) -c -Wall user_interface.c `pkg-config --cflags --libs gtk+-3.0`    
clean:
	$(RM) -rf  *.o cipher
