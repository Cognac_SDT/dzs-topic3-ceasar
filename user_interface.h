#ifndef _USER_INTERFACE_H_
#define _USER_INTERFACE_H_

#include "define.h"
#include "caesar_cipher.h"
#include "thread.h"

/*
** Show dialog window
*/
gboolean dialog_finish (program_data *p_data);
/*
** Show dialog window
*/
gboolean dialog_rescan (program_data *p_data);
/*
** Show dialog window
*/
gboolean refresh_tree_view (program_data *p_data);
/*
** Refesh value and render progress bar, label machine work
*/
gboolean fill (program_data *p_data);
/*
** Update folder directory when user choose folder
*/
gboolean 
fc_choose_folder_set(
    GtkFileChooserButton *chooser, 
    program_data *p_data);
/*
** Scan files in folders and add to task list
*/
gboolean 
b_scan_clicked(
    GtkButton *button, 
    program_data* p_data);
/*
** Signal to begin encrypt files in the "encrypt task list";
*/
gboolean 
b_encrypt_clicked(
    GtkButton *button, 
    program_data* p_data);
/*
** Signal to begin decrypt files in the "decrypt task list";
*/
gboolean 
b_decrypt_clicked(
    GtkButton *button, 
    program_data* p_data);
/*
** Update caesar key when user value changed;
*/
gboolean 
b_caesar_key_changed(
    GtkButton *button, 
    program_data* p_data);
/*
** Show dialog about
*/
gboolean 
b_about_clicked(
    GtkButton *button, 
    program_data* p_data);
/*
** Hide "process finish window dialog"
*/
gboolean 
b_finish_ok_clicked(
    GtkWidget *window, 
    program_data* p_data);
/*
** Hide "rescan window dialog"
*/
gboolean 
b_rescan_ok_clicked(
    GtkWidget *window, 
    program_data* p_data);
/*
** Hide "scan window dialog"
*/
gboolean 
b_scan_ok_clicked(
    GtkWidget *window, 
    program_data* p_data);
/*
** Hide "success scan window dialog"
*/
gboolean 
b_directory_ok_clicked(
    GtkWidget *window, 
    program_data* p_data);
/*
** Hide "about window dialog"
*/
gboolean 
b_about_ok_clicked(
    GtkWidget *window, 
    program_data* p_data);
/*
** Hide "about window dialog"
*/
gboolean 
b_wait_ok_clicked(
    GtkWidget *window, 
    program_data* p_data);
/*
** handle row when user choosed
*/
void 
view_popup_menu_do_remove_encrypt(
    GtkWidget *menuitem, 
    program_data *p_data);
/*
** Create popup menu, call function "view_popup_menu_do_remove_encrypt"
** when user choose
*/
void 
view_popup_menu_list_encrypt(
    GtkWidget *treeview, 
    GdkEventButton *event, 
    program_data *p_data);
/*
** Show popup menu
*/
gboolean 
view_onButtonPressed_list_encrypt(
    GtkWidget *treeview, 
    GdkEventButton *event, 
    program_data *p_data);
/*
** handle row when user choosed
*/
void 
view_popup_menu_do_remove_decrypt(
    GtkWidget *menuitem, 
    program_data *p_data);
/*
** Create popup menu, call function "view_popup_menu_do_remove_decrypt"
** when user choose
*/
void 
view_popup_menu_list_decrypt(
    GtkWidget *treeview, 
    GdkEventButton *event, 
    program_data *p_data);
/*
** Show popup menu
*/
gboolean 
view_onButtonPressed_list_decrypt(
    GtkWidget *treeview, 
    GdkEventButton *event, 
    program_data *p_data);
/*
** Free resources when user close window main
*/
void 
on_window_main_destroy(
    GtkWidget *window_main, 
    program_data *p_data);

#endif