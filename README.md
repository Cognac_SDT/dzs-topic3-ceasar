# DZS Caesar Encryption program

## Sumary

Caesar Encryption is mini project using knowledge of multithreading and semaphore. Purpose is Use semaphore to synchronize resources and synchronize operations (n jobs,2 staff threads, 4 machine threads).

## Idea

I have 4 machines (4 threads cipher machine) that can work simultaneously to encrypt or decrypt files, I hire two staff (2 threads staff) to use them to operate the machines to do the encryption /decryption work flow to the user's request. I will use semaphore to synchronize resources and work between en/decrypt machines and staff:
 - If all 4 machines are running unfinished the employee will sleep until some machine finishes running.
- If there is nothing to do with the machine it will go back to sleep until work is added.

## Installation
Caesar Encryption requires [GTK3+ library](https://developer.gnome.org/gtk3/stable/gtk-getting-started.html) to run.

- Install GTK3+ libraries `sudo apt-get update` and `sudo apt-get install libgtk-3-dev`

## Building for source

1. Open a terminal in the directory containing the project.
2. On terminal run commands: `make` 
3. Run program: `./cipher`
