
#include "thread.h"
#include "caesar_cipher.h"
#include "user_interface.h"

int main(int argc, char *argv[])
{
    int i;
    system("reset");

    pthread_mutex_init(&mutex_staff, NULL);
    pthread_mutex_init(&mutex_machine, NULL);

    /*Creat semaphore*/
    sem_init(&sem_ready, 0, MACHINE_NUMBER);
    sem_init(&sem_task, 0, 0);
    sem_init(&sem_refresh_view, 0, 1);
    sem_init(&sem_dialog_finish, 0, 1);
    sem_init(&sem_dialog_rescan, 0, 1);
    sem_init(&sem_encrypt,0,0);
    sem_init(&sem_decrypt,0,0);
    sem_init(&sem_encrypt_finish,0,0);
    sem_init(&sem_decrypt_finish,0,0);
    if (&sem_ready == SEM_FAILED ||
        &sem_task == SEM_FAILED ||
        &sem_encrypt == SEM_FAILED ||
        &sem_decrypt == SEM_FAILED ||
        &sem_refresh_view == SEM_FAILED ||
        &sem_dialog_finish == SEM_FAILED ||
        &sem_dialog_rescan == SEM_FAILED) {
        perror("Semaphore init failed: ");
    }

    GtkCellRenderer *renderer, *renderer_attr;
    renderer_attr = gtk_cell_renderer_text_new();
    renderer = gtk_cell_renderer_text_new();
    g_object_set(renderer_attr, "ellipsize", PANGO_ELLIPSIZE_MIDDLE, NULL);

    GtkBuilder *builder = NULL;
    program_data *p_data = g_slice_new0(program_data);
    p_data->folder_chooser_path = (char*)calloc(MAX_PATH_LENGTH, sizeof(char));

    /*Create new encrypt list*/
    task_list *new_encrypt_list = NULL;
    new_encrypt_list = (task_list*)calloc(1, sizeof(task_list));
    p_data->encrypt_list = new_encrypt_list;
    p_data->encrypt_list->front = NULL;
    p_data->encrypt_list->rear = NULL;

    /*Create new decrypt list*/
    task_list *new_decrypt_list = NULL;
    new_decrypt_list = (task_list*)calloc(1, sizeof(task_list));
    p_data->decrypt_list = new_decrypt_list;
    p_data->decrypt_list->front = NULL;
    p_data->decrypt_list->rear = NULL;

    p_data->task_in =0;
    p_data->task_out = 0;
    /*Init ceasar key*/
    p_data->caesar_key = 15;

    /*Config UI main*/
    gtk_init(&argc, &argv);
    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "GLADE/dzs_encryption_ui.glade", NULL);
    gtk_builder_connect_signals(builder, p_data);

    p_data->window_main = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    p_data->window_dialog_finish = GTK_WIDGET(gtk_builder_get_object(builder, "window_dialog_finish"));
    p_data->window_dialog_rescan = GTK_WIDGET(gtk_builder_get_object(builder, "window_dialog_rescan"));
    p_data->window_dialog_scan = GTK_WIDGET(gtk_builder_get_object(builder, "window_dialog_scan"));
    p_data->window_dialog_directory = GTK_WIDGET(gtk_builder_get_object(builder, "window_dialog_directory"));
    p_data->window_dialog_about = GTK_WIDGET(gtk_builder_get_object(builder, "window_dialog_about"));
    p_data->window_dialog_wait = GTK_WIDGET(gtk_builder_get_object(builder, "window_dialog_wait"));

    gtk_window_set_title(GTK_WINDOW(p_data->window_main), "DZS Caesar Encryption");
    gtk_window_set_default_size(GTK_WINDOW(p_data->window_main), 950, 450);
    gtk_window_set_default_size(GTK_WINDOW(p_data->window_dialog_finish), 450, 200);
    gtk_window_set_default_size(GTK_WINDOW(p_data->window_dialog_rescan), 450, 200);
    gtk_window_set_default_size(GTK_WINDOW(p_data->window_dialog_scan), 450, 200);
    gtk_window_set_default_size(GTK_WINDOW(p_data->window_dialog_directory), 450, 200);
    gtk_window_set_default_size(GTK_WINDOW(p_data->window_dialog_wait), 450, 200);
    gtk_window_set_default_size(GTK_WINDOW(p_data->window_dialog_about), 550, 250);

    p_data->mb_setting = GTK_MENU_BUTTON(gtk_builder_get_object(builder, "mb_setting"));
    p_data->b_encrypt = GTK_BUTTON(gtk_builder_get_object(builder, "b_encrypt"));
    p_data->b_decrypt = GTK_BUTTON(gtk_builder_get_object(builder, "b_decrypt"));
    p_data->b_scan = GTK_BUTTON(gtk_builder_get_object(builder, "b_scan"));
    p_data->b_about = GTK_BUTTON(gtk_builder_get_object(builder, "b_about"));
    p_data->b_finish_ok = GTK_BUTTON(gtk_builder_get_object(builder, "b_finish_ok"));
    p_data->b_rescan_ok = GTK_BUTTON(gtk_builder_get_object(builder, "b_rescan_ok"));
    p_data->b_scan_ok = GTK_BUTTON(gtk_builder_get_object(builder, "b_scan_ok"));
    p_data->b_about_ok = GTK_BUTTON(gtk_builder_get_object(builder, "b_about_ok"));
    p_data->b_directory_ok = GTK_BUTTON(gtk_builder_get_object(builder, "b_directory_ok"));
    p_data->b_wait_ok = GTK_BUTTON(gtk_builder_get_object(builder, "b_wait_ok"));

    p_data->sb_caesar_key = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "sb_caesar_key"));
    p_data->w_encrypt_list = GTK_WIDGET(gtk_builder_get_object(builder, "treeview_encrypt_list"));
    p_data->w_decrypt_list = GTK_WIDGET(gtk_builder_get_object(builder, "treeview_decrypt_list"));
    p_data->fc_choose_folder = GTK_WIDGET(gtk_builder_get_object(builder, "folderchooserscan"));

    p_data->progress_bar[0] = GTK_WIDGET(gtk_builder_get_object(builder, "progress_bar_1"));
    p_data->progress_bar[1] = GTK_WIDGET(gtk_builder_get_object(builder, "progress_bar_2"));
    p_data->progress_bar[2] = GTK_WIDGET(gtk_builder_get_object(builder, "progress_bar_3"));
    p_data->progress_bar[3] = GTK_WIDGET(gtk_builder_get_object(builder, "progress_bar_4"));

    p_data->machine_task[0] = GTK_LABEL(gtk_builder_get_object(builder, "lb_m_task_1"));
    p_data->machine_task[1] = GTK_LABEL(gtk_builder_get_object(builder, "lb_m_task_2"));
    p_data->machine_task[2] = GTK_LABEL(gtk_builder_get_object(builder, "lb_m_task_3"));
    p_data->machine_task[3] = GTK_LABEL(gtk_builder_get_object(builder, "lb_m_task_4"));
    p_data->machine_state[0] = GTK_LABEL(gtk_builder_get_object(builder, "lb_m_state_1"));
    p_data->machine_state[1] = GTK_LABEL(gtk_builder_get_object(builder, "lb_m_state_2"));
    p_data->machine_state[2] = GTK_LABEL(gtk_builder_get_object(builder, "lb_m_state_3"));
    p_data->machine_state[3] = GTK_LABEL(gtk_builder_get_object(builder, "lb_m_state_4"));

    g_signal_connect(p_data->b_encrypt, "clicked", (GCallback)b_encrypt_clicked, p_data);
    g_signal_connect(p_data->b_decrypt, "clicked", (GCallback)b_decrypt_clicked, p_data);
    g_signal_connect(p_data->b_scan, "clicked", (GCallback)b_scan_clicked, p_data);
    g_signal_connect(p_data->b_about, "clicked", (GCallback)b_about_clicked, p_data);
    g_signal_connect(p_data->b_finish_ok, "clicked", (GCallback)b_finish_ok_clicked, p_data);
    g_signal_connect(p_data->b_rescan_ok, "clicked", (GCallback)b_rescan_ok_clicked, p_data);
    g_signal_connect(p_data->b_scan_ok, "clicked", (GCallback)b_scan_ok_clicked, p_data);
    g_signal_connect(p_data->b_directory_ok, "clicked", (GCallback)b_directory_ok_clicked, p_data);
    g_signal_connect(p_data->b_about_ok, "clicked", (GCallback)b_about_ok_clicked, p_data);
    g_signal_connect(p_data->b_wait_ok, "clicked", (GCallback)b_wait_ok_clicked, p_data);

    g_signal_connect(p_data->fc_choose_folder, "file-set", (GCallback)fc_choose_folder_set, p_data);
    g_signal_connect(p_data->sb_caesar_key, "value-changed", (GCallback)b_caesar_key_changed, p_data);
    g_signal_connect(p_data->w_encrypt_list, "button-press-event", (GCallback)view_onButtonPressed_list_encrypt, p_data);
    g_signal_connect(p_data->w_decrypt_list, "button-press-event", (GCallback)view_onButtonPressed_list_decrypt, p_data);
    g_signal_connect(p_data->window_main, "destroy", (GCallback)on_window_main_destroy, p_data);

    p_data->en_column[0] = gtk_tree_view_column_new_with_attributes("C", renderer, "text", 0, NULL);
    p_data->en_column[1] = gtk_tree_view_column_new_with_attributes("STATE", renderer, "text", 1, NULL);
    p_data->en_column[2] = gtk_tree_view_column_new_with_attributes("ORIGINAL FILE", renderer_attr, "text", 2, NULL);
    p_data->en_column[3] = gtk_tree_view_column_new_with_attributes("SIZE", renderer, "text", 3, NULL);
    p_data->en_column[4] = gtk_tree_view_column_new_with_attributes("PATH", renderer, "text", 4, NULL);
    p_data->de_column[0] = gtk_tree_view_column_new_with_attributes("C", renderer, "text", 0, NULL);
    p_data->de_column[1] = gtk_tree_view_column_new_with_attributes("STATE", renderer, "text", 1, NULL);
    p_data->de_column[2] = gtk_tree_view_column_new_with_attributes("ENCRYPTED FILE", renderer_attr, "text", 2, NULL);
    p_data->de_column[3] = gtk_tree_view_column_new_with_attributes("SIZE", renderer, "text", 3, NULL);
    p_data->de_column[4] = gtk_tree_view_column_new_with_attributes("PATH", renderer, "text", 4, NULL);

    gtk_tree_view_column_set_max_width(p_data->en_column[2],1);
    gtk_tree_view_column_set_expand(p_data->en_column[2],TRUE);
    gtk_tree_view_column_set_max_width(p_data->de_column[2],1);
    gtk_tree_view_column_set_expand(p_data->de_column[2],TRUE);

    for (i = 0; i < 5; i++) {
        gtk_tree_view_append_column(GTK_TREE_VIEW(p_data->w_encrypt_list), p_data->en_column[i]);
        gtk_tree_view_append_column(GTK_TREE_VIEW(p_data->w_decrypt_list), p_data->de_column[i]);
    }
    gtk_tree_view_column_set_visible(p_data->en_column[4], FALSE);
    gtk_tree_view_column_set_visible(p_data->de_column[4], FALSE);
    p_data->liststore_encrypt = gtk_list_store_new(5, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    p_data->liststore_decrypt = gtk_list_store_new(5, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    gtk_tree_view_set_model(GTK_TREE_VIEW(p_data->w_encrypt_list), GTK_TREE_MODEL(p_data->liststore_encrypt));
    gtk_tree_view_set_model(GTK_TREE_VIEW(p_data->w_decrypt_list), GTK_TREE_MODEL(p_data->liststore_decrypt));
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(p_data->liststore_encrypt), 1, GTK_SORT_DESCENDING);
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(p_data->liststore_decrypt), 1, GTK_SORT_DESCENDING);
    
    g_object_unref(p_data->liststore_encrypt);
    g_object_unref(p_data->liststore_decrypt);
    g_object_unref(builder);
    gtk_widget_show(p_data->window_main);

    for (i = 0; i < MACHINE_NUMBER; i++) {
        /*String machine buffer memory allocate*/
        machine_buffer[i].file_path = (char*)calloc(MAX_PATH_LENGTH, sizeof(char));
        machine_buffer[i].file_name = (char*)calloc(MAX_NAME_LENGTH, sizeof(char));
        /*Reset machine rogress*/
        p_data->m_process_info[i].fraction = 0.0;
        p_data->m_process_info[i].total_work = 0.0;
        p_data->m_process_info[i].remain_work = 0.0;
        /*Set init label*/
        gtk_label_set_label(p_data->machine_task[i], "I'm ready...");
        gtk_label_set_label(p_data->machine_state[i], "WAITING WORK");
    }
    /*Creat thread*/
    for (i = 0; i < MACHINE_NUMBER; i++) {
        pthread_create(&p_data->machine_thread[i], NULL, machine_handle, p_data);
    }
    pthread_create(&p_data->staff_encrypt_thread, NULL, staff_handle_encrypt, p_data);
    pthread_create(&p_data->staff_decrypt_thread, NULL, staff_handle_decrypt, p_data);

    /*Run UI main*/
    gtk_main();
    return 0;
}



