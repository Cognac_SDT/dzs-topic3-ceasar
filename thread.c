#include "thread.h"

void *machine_handle(void *gp)
{
    program_data *p_data = (program_data*)gp;
    pthread_t my_tid = pthread_self();
    unsigned short int m_index;
    m_index = machine_index_get(my_tid, p_data);
    /*Create temp node save task info*/
    file_info info_temp;
    info_temp.file_name = (char*)malloc(MAX_NAME_LENGTH * sizeof(char));
    info_temp.file_path = (char*)malloc(MAX_PATH_LENGTH * sizeof(char));
    task_node *p_node;
    while (1)
    {
        sem_wait(&sem_task);
        pthread_mutex_lock(&mutex_machine);
        printf("M-%ld get task - %s\n", my_tid, machine_buffer[p_data->task_out].file_name);
        memset(info_temp.file_name, '\0', MAX_NAME_LENGTH);
        memset(info_temp.file_path, '\0', MAX_PATH_LENGTH);
        strcat(info_temp.file_name, machine_buffer[p_data->task_out].file_name);
        strcat(info_temp.file_path, machine_buffer[p_data->task_out].file_path);
        info_temp.file_size = machine_buffer[p_data->task_out].file_size;
        info_temp.file_state = machine_buffer[p_data->task_out].file_state;
        p_data->task_out = p_data->task_out + 1;
        p_data->task_out = p_data->task_out % MACHINE_NUMBER;
        switch (info_temp.file_state) {
        case ORIGINAL: {
            p_node = task_search(info_temp.file_name, p_data->encrypt_list);
            if (NULL != p_node)p_node->handle_state = ENCRYPTING;
        } break;
        case ENCRYPTED: {
            p_node = task_search(info_temp.file_name, p_data->decrypt_list);
            if (NULL != p_node)p_node->handle_state = DECRYPTING;
        } break;
        default: break;
        }
        sem_wait(&sem_refresh_view);
        p_data->id_gsource_refresh_ui = g_timeout_add (1, (GSourceFunc)refresh_tree_view, p_data);
        pthread_mutex_unlock(&mutex_machine);
        memset(p_data->m_process_info[m_index].name, '\0', MAX_NAME_LENGTH);
        memset(p_data->m_process_info[m_index].state, '\0', MAX_STATE_LENGTH);
        strcat(p_data->m_process_info[m_index].name, info_temp.file_name);
        switch (info_temp.file_state) {
        case ORIGINAL: strcat(p_data->m_process_info[m_index].state, "ENCRYPT"); break;
        case ENCRYPTED: strcat(p_data->m_process_info[m_index].state, "DECRYPT"); break;
        default: break;
        }
        /*Call function to encrypt/decrypt file*/
        en_de_cryption(p_data, &info_temp, m_index);
        memset(p_data->m_process_info[m_index].name, '\0', MAX_NAME_LENGTH);
        memset(p_data->m_process_info[m_index].state, '\0', MAX_STATE_LENGTH);
        strcat(p_data->m_process_info[m_index].name, "I'm ready...");
        strcat(p_data->m_process_info[m_index].state, "WAITING WORK");
        remove(info_temp.file_path);

        pthread_mutex_lock(&mutex_machine);
        if (NULL != p_node)p_node->handle_state = DONE;
        p_data->num_done_work = p_data->num_done_work + 1;
        switch (info_temp.file_state) {
        case ORIGINAL: p_data->num_done_en_work = p_data->num_done_en_work + 1; break;
        case ENCRYPTED: p_data->num_done_de_work = p_data->num_done_de_work + 1; break;
        default: break;
        }
        sem_wait(&sem_refresh_view);
        p_data->id_gsource_refresh_ui = g_timeout_add (1, (GSourceFunc)refresh_tree_view, p_data);
        printf("WORK DONE IS: %d\n", p_data->num_done_work);
        if (p_data->num_done_en_work == p_data->num_total_en_work && p_data->num_total_en_work!=0) {
            sem_post(&sem_encrypt_finish);
            p_data->num_total_en_work = 0;
            p_data->num_done_en_work = 0;        }
        if (p_data->num_done_de_work == p_data->num_total_de_work && p_data->num_total_de_work!=0) {
            sem_post(&sem_decrypt_finish);
            p_data->num_total_de_work = 0;
            p_data->num_done_de_work = 0;        }
        if (p_data->num_done_work == p_data->num_total_work) {
            sem_wait(&sem_dialog_finish);
            p_data->id_gsource_dialog = g_timeout_add (1, (GSourceFunc)dialog_finish, p_data);
            p_data->num_total_work = 0;
            p_data->num_done_work = 0;
            sleep(2);
            g_source_remove(p_data->id_gsource);
        }
        pthread_mutex_unlock(&mutex_machine);
        sem_post(&sem_ready);
    }
    SAFE_FREE(info_temp.file_name);
    SAFE_FREE(info_temp.file_path);
    pthread_exit(NULL);
}

void *staff_handle_decrypt(void *gp)
{
    program_data *p_data = NULL;
    p_data = (program_data*)gp;
    while (1) {
        sem_wait(&sem_decrypt);
        pthread_t my_tid = pthread_self();
        printf("I'M STAFF [%ld]\n", my_tid);
        if (0 != task_list_is_empty(p_data->decrypt_list))
        {
            p_data->num_total_de_work = task_list_count(p_data->decrypt_list);
            p_data->num_total_work = p_data->num_total_work + p_data->num_total_de_work;
            printf("TOTAL WORK IS: %d\n", p_data->num_total_work);
            printf("TOTAL DE WORK IS: %d\n", p_data->num_total_de_work);
            p_data->id_gsource = g_timeout_add (50, (GSourceFunc)fill, p_data);
            task_node *p_node = p_data->decrypt_list->front;
            p_node = p_data->decrypt_list->front;
            while (NULL != p_node) {
                sem_wait(&sem_ready);
                pthread_mutex_lock(&mutex_staff);
                memset(machine_buffer[p_data->task_in].file_name, '\0', MAX_NAME_LENGTH);
                memset(machine_buffer[p_data->task_in].file_path, '\0', MAX_PATH_LENGTH);
                strcat(machine_buffer[p_data->task_in].file_name, p_node->data.file_name);
                strcat(machine_buffer[p_data->task_in].file_path, p_node->data.file_path);
                machine_buffer[p_data->task_in].file_size = p_node->data.file_size;
                machine_buffer[p_data->task_in].file_state = p_node->data.file_state;
                p_node = p_node->next;
                p_data->task_in = p_data->task_in + 1;
                p_data->task_in = p_data->task_in % MACHINE_NUMBER;
                pthread_mutex_unlock(&mutex_staff);
                sem_post(&sem_task);
            }
            printf("STAFF DECRYPT WORK DONE... WAITING MACHINE FINISH RUN\n");
            //pthread_cond_wait(&cond_decrypt_finish, &mutex);
            sem_wait(&sem_decrypt_finish);
            task_list_free(p_data->decrypt_list);
            printf("DECRYPT LIST DONE!\n");
        } else {
            sem_wait(&sem_dialog_rescan);
            p_data->id_gsource_dialog_rescan = g_timeout_add (1, (GSourceFunc)dialog_rescan, p_data);
        }
    }
    pthread_exit(NULL);
}

void *staff_handle_encrypt(void *gp)
{
    program_data *p_data = NULL;
    p_data = (program_data*)gp;
    while (1) {
        sem_wait(&sem_encrypt);
        pthread_t my_tid = pthread_self();
        printf("I'M STAFF [%ld]\n", my_tid);
        if (0 != task_list_is_empty(p_data->encrypt_list)) {
            p_data->num_total_en_work = task_list_count(p_data->encrypt_list);
            p_data->num_total_work = p_data->num_total_work + p_data->num_total_en_work;
            printf("TOTAL WORK IS: %d\n", p_data->num_total_work);
            printf("TOTAL EN WORK IS: %d\n", p_data->num_total_en_work);
            p_data->id_gsource = g_timeout_add (50, (GSourceFunc)fill, p_data);
            task_node *p_node = p_data->encrypt_list->front;
            p_node = p_data->encrypt_list->front;
            while (NULL != p_node) {
                sem_wait(&sem_ready);
                pthread_mutex_lock(&mutex_staff);
                memset(machine_buffer[p_data->task_in].file_name, '\0', MAX_NAME_LENGTH);
                memset(machine_buffer[p_data->task_in].file_path, '\0', MAX_PATH_LENGTH);
                strcat(machine_buffer[p_data->task_in].file_name, p_node->data.file_name);
                strcat(machine_buffer[p_data->task_in].file_path, p_node->data.file_path);
                machine_buffer[p_data->task_in].file_size = p_node->data.file_size;
                machine_buffer[p_data->task_in].file_state = p_node->data.file_state;
                p_node = p_node->next;
                p_data->task_in = p_data->task_in + 1;
                p_data->task_in = p_data->task_in % MACHINE_NUMBER;
                pthread_mutex_unlock(&mutex_staff);
                sem_post(&sem_task);
            }
            printf("STAFF ENCRYPT WORK DONE... WAITING MACHINE FINISH RUN\n");
            //pthread_cond_wait(&cond_encrypt_finish, &mutex);
            sem_wait(&sem_encrypt_finish);
            task_list_free(p_data->encrypt_list);
            printf("ENCRYPT LIST DONE!\n");
        } else {
            sem_wait(&sem_dialog_rescan);
            p_data->id_gsource_dialog_rescan = g_timeout_add (1, (GSourceFunc)dialog_rescan, p_data);
        }
    }
    pthread_exit(NULL);
}